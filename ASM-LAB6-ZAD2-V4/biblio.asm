.386
.MODEL FLAT, STDCALL
	STD_INPUT_HANDLE equ -10
	STD_OUTPUT_HANDLE equ -11
	GetStdHandle PROTO :DWORD
	atoi  PROTO :DWORD
	wsprintfA PROTO C :VARARG
	WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
	ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
.data
	
	cout		dd ?
	cin		    dd ?
	tekst       db "Wprowadz liczbe [%i] : ",0
	rozmiart    db $ - tekst
	wynik       db "Wynik to: %u",0
	rwynik      db $ - wynik
	bufor		db 128 DUP(?)
	bufor2      db 10 DUP(?)
	tablica		db 10 DUP(0)
	liczbaZ		dd ?
	liczba		dd ?	
	zmienna     db ?	
	licznik     db 1
	suma		dw 0
.code
procedurka proc
	invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov cout, EAX
	
	invoke GetStdHandle, STD_INPUT_HANDLE
	mov cin, EAX
	mov ECX, 4
		mov EDI, OFFSET tablica
	Pentela:
	push ECX
	push EDI
	invoke wsprintfA, OFFSET bufor, OFFSET tekst, licznik
	invoke WriteConsoleA, cout, OFFSET bufor, rozmiart, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor2, 8, OFFSET liczbaZ, 0
	lea EBX, bufor2
	mov EDI, liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
	invoke atoi, OFFSET bufor2
	
	pop EDI
	stosb
	inc licznik
	pop ECX
	cmp ECX,1d
	dec ECX
	jne Pentela
	mov ECX, 4
	mov ESI, OFFSET tablica
	wys:
	push ECX
;	mov EDI, OFFSET bufor
	
	mov EAX,0
	lodsb
	mov bx, suma
	add ax, bx
	mov suma, ax

	pop ECX
	cmp ECX,1d
	dec ECX
	jne wys
	mov ECX, 4
	mov ESI, OFFSET tablica
	invoke wsprintfA, OFFSET bufor2, OFFSET wynik, suma
	invoke WriteConsoleA, cout, OFFSET bufor2, rwynik, OFFSET liczba,0
		
	
	ret
procedurka endp
atoi proc uses esi edx inputBuffAddr:DWORD
	mov esi, inputBuffAddr
	xor edx, edx
	xor EAX, EAX
	mov AL, BYTE PTR [esi]
	cmp eax, 2dh
	je parseNegative

	.Repeat
		
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0
	mov EAX, EDX
	jmp endatoi

	parseNegative:
	inc esi
	.Repeat
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0

	xor EAX,EAX
	sub EAX, EDX
	jmp endatoi

	endatoi:
	ret
atoi endp
END